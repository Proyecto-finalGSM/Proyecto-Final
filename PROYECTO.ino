
 /* 
    ARDUINO UNO     SIM800L
      D11             TX 
      D12          RX
      RESET           RST
      GND             GND
  */
  
#include <SoftwareSerial.h>
  
//Pulsador para enviar el Mensaje
#define SIM800_PIR 8

int contador = 0;
 
//Se declara los pines en el arduino Uno
SoftwareSerial MOD_SIM800L(11,12 );
 
void setup() {
  //Se establece la velocidad para el puerto serie
  Serial.begin(115200);
  while(!Serial);
   
  //Velocidad de trabajo entre el Arduino Uno y el Modulo SIM800L
  MOD_SIM800L.begin(115200);
  delay(1000);
 
  pinMode (SIM800_PIR, INPUT);
 }
 
void loop() {

  if((digitalRead(SIM800_PIR) == 1)&&(contador == 0)){
  Serial.println("Configuracion Completa!");
  Serial.println("Enviando SMS...");
   
  //Se establece el formato de SMS en ASCII
  MOD_SIM800L.write("AT+CMGF=1\r\n");
  delay(1000);
 
  //Enviar comando para un nuevos SMS al numero establecido
  MOD_SIM800L.write("AT+CMGS=\"+51942153303\"\r\n");
  delay(1000);
   
  //Enviar contenido del SMS
  MOD_SIM800L.write("ALERTA INTRUSO");
  delay(1000);
   
  //Enviar Ctrl+Z 
  MOD_SIM800L.write((char)26);
  delay(1000);
     
  Serial.println("Mensaje enviado!");
  }
}
